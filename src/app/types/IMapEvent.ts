import { ICoordinates } from './ICoordinates';
export interface IMapEvent {
    coords: ICoordinates;
}
