import { ICoordinates } from './ICoordinates';

export interface ITechServiceSpecialist {
    name: string;
    surname: string;
    phone: string;
    coords: ICoordinates;
}
