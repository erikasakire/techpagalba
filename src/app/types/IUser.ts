import { ICoordinates } from './ICoordinates';

interface IUser {
    plateNumber: string;
    coords: ICoordinates;
}

export { IUser }
