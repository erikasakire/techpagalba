import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { Router } from '@angular/router';
import { ICoordinates } from '../types/ICoordinates';
import { sg } from '../utils/safeGet';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public centerCoords: ICoordinates = { lat: 54.8985207, lng: 23.9035965 };
  public userCoords: ICoordinates = undefined;
  public error: string = undefined;

  /** Error messages */
  private errorMessage = {
    markerNotSet: 'Prašome nurodyti savo vietą žemėlapyje'
  };

  constructor(
    private mainService: MainService,
    private router: Router
  ) { }

  public ngOnInit(): void {
    /** Redirect to login if user car plate number is not set */
    if (!sg(() => this.mainService.getUser().plateNumber)) {
      console.log('redirect to login');
      this.router.navigateByUrl('login');
    }

    this.getLocation();
  }

  /**
   * Sets user curent coordinates
   */
  private setUserCoordinates(coords: ICoordinates): ICoordinates {
    this.userCoords = coords;
    return coords;
  }
  private setCenterCoordinates(coords: ICoordinates): ICoordinates {
    this.centerCoords = coords;
    return coords;
  }


  /**
   * Gets currect user location.
   */
  private getLocation(): void {
    try {
      if (window.navigator && window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(
          position => {
            const c: ICoordinates = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            this.setCenterCoordinates(c);
            this.setUserCoordinates(c);
            console.log('Location received');
          },
          error => console.log(error)
        );
      }
    } catch (error) {
      console.error(error);
    }
  }
  /**
   * Listens to map marker location change.
   * @param event new marker coordinates
   */
  public handleLocationChange(event: ICoordinates): void {
    this.setUserCoordinates(event);
    this.checkForErrors();
  }
  /**
   * Handles click event of continue button.
   */
  handleClick(): void {
    if (this.checkForErrors()) {
      this.mainService.setUserCoordinates(this.userCoords);
      this.router.navigateByUrl('track');
    }
  }

  /**
   * Check if all constraints are met.
   * @returns boolean. True if all constraints is met, false othervise.
   */
  checkForErrors(): boolean {
    console.log(this.userCoords);
    if (!this.userCoords) {
      this.error = this.errorMessage.markerNotSet;
      return false;
    }

    this.error = undefined;
    return true;
  }
}
