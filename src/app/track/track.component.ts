import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { ICoordinates } from '../types/ICoordinates';
import { Router } from '@angular/router';
import { ITechServiceSpecialist } from '../types/ITechServiceSpecialist';
import { sg } from '../utils/safeGet';
import { IUser } from '../types/IUser';
import * as moment from 'moment';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {
  public user: IUser;
  public specialist: ITechServiceSpecialist;
  public time = moment().add(10, 'minutes');

  constructor(
    private mainService: MainService,
    private router: Router
  ) { }

  ngOnInit() {
    /** Redirecting if user coordinates are not set */
    if (!sg(() => this.mainService.getUser().coords)) {
      this.router.navigateByUrl('');
    }

    this.user = this.mainService.getUser();
    this.specialist = this.mainService.getTechSpecialist();

    setInterval(() => this.getSpecialistLocation(), 1000);
  }

  getSpecialistLocation(): void {
    this.specialist = this.mainService.getTechSpecialist();
  }
}
