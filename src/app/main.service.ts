import { Injectable } from '@angular/core';
import { ICoordinates } from './types/ICoordinates';
import { ITechServiceSpecialist } from './types/ITechServiceSpecialist';
import { IUser } from './types/IUser';
import { sg } from './utils/safeGet';

@Injectable({
  providedIn: 'root'
})
export class MainService {
  private user: IUser = {
    plateNumber: undefined,
    coords: undefined
  };
  private specialists: ITechServiceSpecialist = {
      name: 'Jonas',
      surname: 'Jonaitis',
      phone: '+37060000000',
      coords: {
        lat: 54.895325,
        lng: 23.949013
      }
  };
  private arrived: boolean = false;

  public getUser(): IUser {
    return this.user;
  }
  public setCarPlateNumber(carPlateNumber: string): string {
    this.user.plateNumber = carPlateNumber;
    return carPlateNumber;
  }
  public setUserCoordinates(userCoords: ICoordinates): ICoordinates {
    this.user.coords = userCoords;
    return userCoords;
  }


  public getTechSpecialist(): ITechServiceSpecialist {
    if (!this.arrived) {
      this.specialists.coords = this.updateSpecialistLocation(this.specialists.coords);
      if (sg(() => this.round(this.diff('lat', this.user.coords, this.specialists.coords), 4) === 0
                && this.round(this.diff('lng', this.user.coords, this.specialists.coords), 4) === 0,
          false)) {
            this.arrived = true;
      }
    }
    return this.specialists;
  }


  private speed = 0.00005;
  private diff(axis: 'lat' | 'lng', a: ICoordinates, b: ICoordinates): number {
    switch(axis) {
      case 'lat': return sg(() => a.lat, 0) - sg(() => b.lat, 0);
      case 'lng': return sg(() => a.lng, 0) - sg(() => b.lng, 0);
      default: return 0;
    }
  }
  private round(value: number, precision: number): number {
    const prec = Math.pow(10, precision);
    return Math.round(value * prec) / prec;
  }

  private updateSpecialistLocation(old: ICoordinates): ICoordinates {
    const userCoords = sg(() => this.user.coords, old);
    return ({
      lat: old.lat + (this.diff('lat', userCoords, old) < 0 ? -1 : 1) * Math.random() * this.speed,
      lng: old.lng + (this.diff('lng', userCoords, old) < 0 ? -1 : 1) * Math.random() * this.speed
    });
  }
}
