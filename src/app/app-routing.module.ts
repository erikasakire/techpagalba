import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { TrackComponent } from './track/track.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'track', component: TrackComponent },
  { path: '', component: MainComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
