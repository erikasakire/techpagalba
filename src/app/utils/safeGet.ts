function sg<T>(getter: () => T, defaultValue: T = undefined): T {
    try {
        return getter();
    } catch {
        return defaultValue;
    }
}

export { sg }
