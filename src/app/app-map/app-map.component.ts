/// <reference types="@types/googlemaps" />
import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';
import { IMapEvent } from '../types/IMapEvent';
import { ICoordinates } from '../types/ICoordinates';

@Component({
  selector: 'app-app-map',
  templateUrl: './app-map.component.html',
  styleUrls: ['./app-map.component.scss']
})
export class AppMapComponent implements OnInit, OnChanges {
  @Input() public centerCoords: ICoordinates;
  @Input() public userCoords: ICoordinates;
  @Input() public techSpecialistcoords?: ICoordinates;
  @Output() public userCoordsChange = new EventEmitter<ICoordinates>();

  public zoom: number;
  public mapbounds: google.maps.LatLngBounds | boolean = false;

  ngOnInit() {
    this.changeZoom();

    if (this.techSpecialistcoords) {
      this.mapbounds = new google.maps.LatLngBounds();
      this.mapbounds.extend(new google.maps.LatLng(this.userCoords.lat, this.userCoords.lng));
      this.mapbounds.extend(new google.maps.LatLng(this.techSpecialistcoords.lat, this.techSpecialistcoords.lng));
    }
  }
  ngOnChanges() {
    this.changeZoom();
  }

  /**
   * Changes map zoom level, depending on if marker was set.
   */
  changeZoom(): void {
    this.zoom = (this.userCoords) ? 13 : 8;
  }

  /**
   * Handles markerOnDrag and mapClick events.
   * @param event IMapEvent type object
   */
  handleMapEvent(event: IMapEvent): void {
    this.centerCoords =
      this.userCoords = {
        lat: event.coords.lat,
        lng: event.coords.lng
      };

    this.userCoordsChange.emit(this.userCoords);
  }
}
