import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public inputValue: string;
  public error: string = undefined;

  /** Error messages */
  private errorMessage = {
    InputValueNotSet: 'Prašome įvesti automobilio numerius.'
  };

  constructor(
    private mainService: MainService,
    private router: Router
  ) { }

  /**
   * Handles click event of continue button.
   */
  handleClick(): void {
    if (this.checkForErrors()) {
      this.mainService.setCarPlateNumber(this.inputValue.toLocaleUpperCase());
      this.router.navigateByUrl('');
    }
  }

  /**
   * Check if all constraints are met.
   * @returns boolean. True if all constraints is met, false othervise.
   */
  checkForErrors(): boolean {
    if (!(this.inputValue && this.inputValue.length > 0)) {
      this.error = this.errorMessage.InputValueNotSet;
      return false;
    }

    this.error = undefined;
    return true;
  }
}
